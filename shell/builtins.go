package shell

import (
	"strconv"
)

func DefaultBuiltins() map[string]Builtin {
	return map[string]Builtin{
		"exit": BuiltinExit,
	}
}

func BuiltinExit(s *State, cmd *Cmd) {
	if len(cmd.argv) >= 2 {
		code, err := strconv.Atoi(cmd.argv[1])
		if err == nil {
			s.Overrides.Exit(int(code))
		}
	}
	s.Overrides.Exit(0)
}
