package shell

import "os"

// TODO: rename file to types.go ?

type Cmd struct {
	os.ProcAttr
	name string
	argv []string
}

type Builtin = func(*State, *Cmd)

type State struct {
	IsInteractive bool

	Builtins  map[string]func(*State, *Cmd)
	Aliases   map[string]string
	variables map[string]Var

	// Special variables
	varExitStatus int // $?

	Overrides Overrides

	parent *State
}

type Var struct {
	Value string
}

type Overrides struct {
	Chdir   func(dir string) error
	Environ func() []string
	Exit    func(code int)
}

func DefaultState() State {
	return State{
		Builtins: DefaultBuiltins(),
		Aliases:  map[string]string{},

		Overrides: DefaultOverrides(),
	}
}

func DefaultOverrides() Overrides {
	return Overrides{
		Chdir:   os.Chdir,
		Environ: os.Environ,
		Exit:    func(code int) { panic(exitError{code}) },
	}
}

// Errors propagated using panic
type exitError struct{ code int }
