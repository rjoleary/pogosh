package main

import (
	"fmt"
	"os"

	"github.com/rjoleary/pogosh/shell"
)

func main() {
	// TODO: not standard
	file := "/dev/stdin"
	if len(os.Args) > 1 {
		file = os.Args[1]
	}

	state := shell.DefaultState()
	code, err := state.RunFile(file)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	os.Exit(code)
}
