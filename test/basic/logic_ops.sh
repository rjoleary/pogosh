# [DESCRIPTION]
# Test logic operators and short-circuiting

# [STDOUT]
# a
# b
# c
# d
# e
# f
# g
# 0
# 1
#

echo a && echo b && echo c
echo d || echo x1 || echo x2

false && echo x3 || echo e
false || echo f && echo g

# TODO: (exit 2) || (exit 3) || (exit 4); echo $?
# TODO: (exit 0) && (exit 5) && (exit 6); echo $?

! false; echo $?
! true; echo $?
# TODO: ! (exit 2); echo $?
