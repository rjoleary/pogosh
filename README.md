# POSIX Golang Shell

Goals:

* POSIX-2017 compliant
* No imports besides standard library

## POSIX Exception

Everything is POSIX except for:

```
( sleep 10 ) &
PID=$!
kill $PID
echo $PID
```

Because Go does not support forking, asynchronous subshells run in a goroutine
within the same process. Goroutines do not have PIDs so the value of `$!` is
"goN" where `N` is a unique id for that goroutine. `kill` is a builtin and
understands this notation. However, using `$!` (or a variable deriving from it)
in any other command is not POSIX compliant.

Setting `POGOSH_WBANG=1` prints a warning to stderr whenever the above
restriction is violated.

For similar reasons, umask is also not POSIX compliant.

## Scheduled features

* Error messages with line/column
* Interactive mode
* Job control
* Completions

## Links

* POSIX: http://pubs.opengroup.org/onlinepubs/9699919799/
* C: http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf
