# Peculiars

Some interesting shell script syntax.

Grammar changing alias:

    alias x="echo \""

Umask only applies to subshell:

    (umask 022 && cmd)

Regex for newlines separating commands:

    n*|n*(c(n*nc)*n*)

Misc:

- Case without semicolon
- Alias take effect immediately -- unspecified?
- Unicode / character encoding
- y=a echo $((1?2:y))
- Check suid / sgid
